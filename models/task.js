// Import mongoose module
const mongoose = require("mongoose");

// Create mongoose Schema
const taskSchema = new mongoose.Schema({

    name: String,
    status: {
        type: String,
        default: "pending"
    }

});

// Import mongoose Model
// "module.exports" is a way for JS to treat this value as a package that can be used by other files
module.exports = mongoose.model("Task", taskSchema);