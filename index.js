// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This will allow us to use all the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@course-booking.2xxzg.mongodb.net/B157_to_do?retryWrites=true&w=majority", 
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to cloud database - mongoDB."));

// Add the task route
// Allow all the task routes created in the "taskRoute.js" file to use the "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, () => `Server is running at port ${port}.`)