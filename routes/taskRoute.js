// Contains all of the endpoints of our application

// We need to use express' Router() function to achieve this
const express = require("express");

// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController")

// Route to get all the tasks
router.get("/", (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a task
router.post("/", (req, res) => {
    console.log(req.body);
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for deleting a task
router.delete("/:id", (req, res) => {
    console.log(req.params)
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(`Task ${resultFromController.name} is deleted!`));
});

// Route to update a task
router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});
/* router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(`Task ${req.params.id} is now updated to ${resultFromController.name}`));
}); */


/* ACTIVITY - Session 31

Instructions:

1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.

*/

// Route to get a specific task
router.get("/:id", (req, res) => {
    taskController.findTask(req.params.id).then(resultFromController => res.send(resultFromController))
});

// Route to update status of a task
router.put("/:id/complete", (req, res) => {
    taskController.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


// Use "module.exports" to export the roter object to use in the index.js
module.exports = router;